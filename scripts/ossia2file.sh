#!/bin/bash 

FILENAME=$1

gst-launch-1.0 shmdatasrc socket-path=/tmp/score_shm_video ! videoconvert ! "video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)60/1, format=(string)YUY2" ! videoconvert ! jpegenc quality=100 ! avimux ! filesink location=$FILENAME
