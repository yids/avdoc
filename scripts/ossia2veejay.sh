#!/bin/bash

WIDTH=720
HEIGHT=576
NUM=$1


GST_DEBUG=4 gst-launch-1.0 shmdatasrc socket-path=/tmp/video$NUM \
        ! glupload \
        ! glcolorconvert \
        ! video/x-raw\(memory:GLMemory\),format=YUY2 \
        ! gldownload \
        ! videoscale \
        ! video/x-raw, width=$WIDTH, height=$HEIGHT  \
        ! queue  \
        ! v4l2sink device=/dev/video$NUM


