#!/bin/bash

WIDTH=640
HEIGHT=480
VIDEO_DEV1=/dev/video1
VIDEO_DEV2=/dev/video2

GST_DEBUG=3 gst-launch-1.0 shmdatasrc socket-path=/tmp/video1 \
        ! videoconvert \
        ! video/x-raw,format=NV12 \
        ! videoscale \
        ! video/x-raw, width=$WIDTH, height=$HEIGHT  \
        ! queue  \
        ! v4l2sink device=$VIDEO_DEV1 &

GST_DEBUG=3 gst-launch-1.0 shmdatasrc socket-path=/tmp/video2 \
        ! videoconvert \
        ! video/x-raw,format=NV12 \
        ! videoscale \
        ! video/x-raw, width=$WIDTH, height=$HEIGHT  \
        ! queue  \
        ! v4l2sink device=$VIDEO_DEV2 &
